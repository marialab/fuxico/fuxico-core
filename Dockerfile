FROM alpine:3.5

RUN apk add --update bash python2 lighttpd perl php5 php5-cgi openrc git make && \
    mkdir /opt

ADD piratebox /opt/piratebox
ADD piratebox/share/ /opt/piratebox/www
ADD build /opt/piratebox/bin/

RUN sed -i -e 's/^USE_APN="yes"/USE_APN="no"/' \
           -e 's/^DO_IFCONFIG="yes"/DO_IFCONFIG="no"/' \
           -e 's/^USE_DNSMASQ="yes"/USE_DNSMASQ="no"/' \
           /opt/piratebox/conf/piratebox.conf

COPY docker/start_fuxico.sh /opt/piratebox/bin/

# Do PirateBox init
RUN chmod -R +x /opt/piratebox/bin/* ; \
    chmod -R +x /opt/piratebox/init.d/* ; \
    chown -R lighttpd /opt/piratebox/share ; \
    chown -R lighttpd /opt/piratebox/www ; \
    chmod -R 777 /opt/piratebox/tmp /opt/piratebox/share; \
    adduser -D -g '' alarm

# Lighttpd Port
EXPOSE 80
# Droopy Port  (soon absolete)
EXPOSE 8080

# Share space
VOLUME ["/opt/piratebox/www/"]

CMD [ "/opt/piratebox/bin/start_fuxico.sh" ]
