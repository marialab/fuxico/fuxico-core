#!/bin/sh

# Starts deamon for global sementeira service
#  requires  piratebox.conf as first parameter
#
#  Paty Morimoto - 2019
#  Licenced with GPL-3

. $1

echo 'UPLOAD FOLDER ' $UPLOADFOLDER

cd $PIRATEBOX_FOLDER/python_lib

export UPLOADFOLDER=$UPLOADFOLDER

exec $PIRATEBOX_FOLDER/bin/bottle.py -b $HOST:$SEMENTEIRA_PORT sementeira:app
