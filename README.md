# Fuxico

__Version:__ Fuxico_Latest_rpi3+.img

## Docker

Instalando o docker: [[https://docs.docker.com/install/]]

Para executar: [[https://gitlab.com/marialab/fuxico/fuxico-image/blob/8c8c000875b9dc10269f0927c9792d224f5260a9/README.md]]
