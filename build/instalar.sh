#!/bin/bash
# Script de instalacao da Fuxico
# Carl@Vedetas 2018
echo "Bem vinda a instalacao da sua fuxico. Vamos fazer algumas perguntas que voce
deve responder e depois apertar <enter>, ok?"
echo

# le a senha e altera
read -s -p "Passo 1>> Digite uma nova senha para o usuario alarm (nao esqueca dela): " password
echo
read -s -p "Digite novamente a senha: " password2

# Verifica se as senhas sao iguais e altera elas
while [ "$password" != "$password2" ];
do
    echo
    echo "As senhas devem ser iguais. Por favor tente novamente."
    read -s -p "Passo 1 >> Digite uma nova senha para o usuario alarm (nao esqueca dela): " password
    echo
    read -s -p "Digite novamente a senha: " password2
done

echo "alarm:$password" | chpasswd
echo 
echo "Ok, sua senha foi alterada."
echo


# Rodas de conversa
echo
read -p "Passo 2 >> Voce gostaria de habilitar as Rodas de conversa? Essa opcao criara uma area adicional na sua fuxico para que as pessoas possam discutir temas em um forum publico ('S' para SIM, 'N' para nao): " rodass
if [ "$rodass" == 'S' ] || [ "$rodass" == 's' ] ;
then
	/opt/piratebox/bin/board-autoconf.sh
echo
fi
echo

#Script de armazenamento da piratebox
read -p "Passo 3 >> Voce precisa decidir se ira guardar os arquivos da fuxico no cartao SD ou em um pendrive ou HD externo. (Digite '1' para Cartão SD e '2' para USB): " arquivoss
echo
if [ "$arquivoss" -eq 2 ];then
	read -p "Para utilizar o armazenamento de arquivos em um pen drive ou HD externo USB voce deve conecta-lo agora na Raspberry Pi e garantir que suas luzes estejam piscando. Esta tudo conectado? (Pressione 'S' para continuar e 'N' para voltar atrás): " usb
		if [ $usb == "S" ] || [ $usb == "s" ] ;
		then
		/opt/piratebox/rpi/bin/usb_share.sh
		fi
		if [ $usb != "S" ] && [ $usb != "s" ];
                then
		echo
                echo "Volte novamente quando decidir seu armazenamento"
                fi
fi
if [ "$arquivoss" -eq 1 ];then
	read -p "Agora iremos estender a partição da fuxico para ocupar todo o resto do seu cartão SD. Pressione 'S' para continuar: " cartaosd
		if [ $cartaosd == "S" ] || [ $cartaosd == "s" ];then
		/opt/piratebox/rpi/bin/sdcard_share.sh
		fi
		if [ "$cartaosd" != "S" ] && [ "$cartaosd" != "s" ];then
		echo
		echo "Volte novamente quando decidir seu armazenamento"
		fi
fi
if [ "$arquivoss" -ne 1 ] && [ "$arquivoss" -ne 2 ]
then
echo "Volte novamente quando decidir seu armazenamento";
fi
echo
echo "Parabens! Sua Fuxico foi instalada com sucesso e esta pronta para uso!!!"
