#!/bin/bash
# Script de instalacao da Fuxico
# @Vedetas 2019
if [[ -z "$DEBUG" ]] || [ "$DEBUG" == 'false' ]; then
	set -e
else
	set -exv
fi

INSTALL_CONF=/opt/piratebox/bin/.install.properties

# Alterar a senha do usuario
function alterarSenhaUsuario(){
	if [[ -z "$SENHA_USUARIO" ]]; then
		echo "Empty SENHA_USUARIO"
		return -1
	else
		echo "alarm:$SENHA_USUARIO" | chpasswd
		echo "OK, sua senha foi alterada."
	fi
}
# Rodas de conversa
function habilitarRoda(){
	if [[ -z "$HABILITAR_RODA" ]] || [[ -z "$SENHA_RODA" ]] ; then
		echo "Empty HABILITAR_RODA or SENHA_RODA"
	else
		if [ "$HABILITAR_RODA" == true ] || [ "$HABILITAR_RODA" == 'true' ] ;
		then
			/opt/piratebox/bin/board-autoconf.sh
			echo OK, Roda de conversa habilitada
		fi
	fi
}

# #Script de armazenamento da piratebox
function escolherArmazenamento(){
	if [[ -z "$TIPO_ARMAZENAMENTO" ]]; then
		echo "Empty TIPO_ARMAZENAMENTO"
		return -1
	else
		case $TIPO_ARMAZENAMENTO in
			USB) /opt/piratebox/rpi/bin/usb_share.sh false;;
			SDCARD) /opt/piratebox/rpi/bin/sdcard_share.sh;;
			BOTH) /opt/piratebox/rpi/bin/sdcard_share.sh; /opt/piratebox/rpi/bin/usb_share.sh true;;
		esac
	fi
}

#Script de pitacos da piratebox
# * CONTENT_SEMENTEIRA: all, autonomia, sexualidade, dicas, tenologias-digitais, saude, agroecologia ou none

function escolherPitacos(){
	if [[ -z "$CONTENT_SEMENTEIRA" ]]; then
		echo "Empty CONTENT_SEMENTEIRA"
		return -1
	else
		case $CONTENT_SEMENTEIRA in
			all) unzip /opt/piratebox/pitacos.zip -d /opt/piratebox/share/;;
			autonomia) unzip /opt/piratebox/pitacos.zip 'autonomia/' -d /opt/piratebox/share/;;
			sexualidade) unzip /opt/piratebox/pitacos.zip 'sexualidade/' -d /opt/piratebox/share/;;
			dicas) unzip /opt/piratebox/pitacos.zip 'dicas/' -d /opt/piratebox/share/;;
			tenologias-digitais) unzip /opt/piratebox/pitacos.zip 'tenologias-digitais/' -d /opt/piratebox/share/;;
			saude) unzip /opt/piratebox/pitacos.zip 'saude/' -d /opt/piratebox/share/;;
			agroecologia) unzip /opt/piratebox/pitacos.zip 'agroecologia/' -d /opt/piratebox/share/;;""
			none) echo "Nenhum conteudo será instalado"
		esac
	fi
	rm /opt/piratebox/pitacos.zip
}

if [[ -f "$INSTALL_CONF" ]]; then
    echo "$INSTALL_CONF exist. Load variables..."
	source $INSTALL_CONF
fi

alterarSenhaUsuario
habilitarRoda
escolherArmazenamento
escolherPitacos

echo "Parabens! Sua Fuxico foi instalada com sucesso e esta pronta para uso!!!"